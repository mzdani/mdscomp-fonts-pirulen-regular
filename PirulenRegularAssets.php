<?php
namespace mdscomp\pirulenregular;

use yii\web\AssetBundle;

class PirulenRegularAssets extends AssetBundle {
	public $sourcePath = '@vendor/mdscomp/pirulen-regular';

	public $css = [
		'assets/pirulen-regular.css',
	];
}
