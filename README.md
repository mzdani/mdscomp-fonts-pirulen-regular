Pirulen Regular Fonts for Yii2
=======================

[![Latest Stable Version](https://poser.pugx.org/mdscomp/pirulen-regular/v/stable.svg)](https://packagist.org/packages/mdscomp/pirulen-regular) [![Total Downloads](https://poser.pugx.org/mdscomp/pirulen-regular/downloads.svg)](https://packagist.org/packages/mdscomp/pirulen-regular) [![Monthly Downloads](https://poser.pugx.org/mdscomp/pirulen-regular/d/monthly.png)](https://packagist.org/packages/mdscomp/pirulen-regular) [![Daily Downloads](https://poser.pugx.org/mdscomp/pirulen-regular/d/daily.png)](https://packagist.org/packages/mdscomp/pirulen-regular) [![Latest Unstable Version](https://poser.pugx.org/mdscomp/pirulen-regular/v/unstable.svg)](https://packagist.org/packages/mdscomp/pirulen-regular) [![License](https://poser.pugx.org/mdscomp/pirulen-regular/license.svg)](https://packagist.org/packages/mdscomp/pirulen-regular)

You can add Pirulen Regular Fonts with Yii2.

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require mdscomp/pirulen-regular "dev-master"
```

or add

```
"mdscomp/pirulen-regular": "*"
```

to the require section of your `composer.json` file.



Usage
-----

Register assets into your view.


```
use mdscomp\PirulenRegularAssets;

PirulenRegularAssets::register($this);
```